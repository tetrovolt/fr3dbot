const parseQuestion = require("./_parseQuestion");

module.exports = function qotdAdd(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (this.bot.qotd.add(guild.id, ...parseQuestion(args, this.bot))) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.args = "<question>: <emoji> <answer> [emoji <answer> [emoji <answer>...]]";
module.exports.description = "Add a question to automatic QOTD queue";
module.exports.admin = true;
