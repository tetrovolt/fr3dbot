const {msgEditTimeout} = global.config;

module.exports = class UndoneCommands {
  constructor() {
    this.cache = {};
  }

  checkAddMessage(msgId, add=true) {
    let exists = false;
    if (msgId in this.cache) {
      clearTimeout(this.cache[msgId]);
      exists = true;
    }

    if (add) {
      this.cache[msgId] = setTimeout(() => {
        delete this.cache[msgId];
      }, msgEditTimeout);
    }

    return exists;
  }

  removeMessage(msgId) {
    if (!(msgId in this.cache)) return;

    clearTimeout(this.cache[msgId]);
    delete this.cache[msgId];
  }
};
